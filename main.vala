//valac --pkg sdl2 --pkg SDL2_gfx main.vala grille.vala burger.vala
using SDL;

void main(){
	SDL.init();
	var win = new Window("Puissance 4",720, 650);
	var grille = new Grille();

	Event event;

	while(win.is_open())
	{
		//evenement
		Event.poll(out event);
		if(event.type == EventType.QUIT){
			win.close();
		}
		grille.event(ref event);

		//dessin
		win.clear();
		grille.draw(ref win);
		win.present();
	}
	SDL.quit();
}
