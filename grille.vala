//valac --pkg sdl2 --pkg SDL2_gfx main.vala burger.vala jeton.vala grille.vala

const int DBC = 80; 

class Grille : Object {
	public Grille () {
		m_curseur = true;
		m_red_can_play = true;
		is_falling = false;

		m_t_grille = new Texture("data/grille.bmp");
		m_t_grille2 = new Texture("data/grille2.bmp");

		m_t_jetonr = new Texture("data/jetonr.bmp");
		m_t_jetonj = new Texture("data/jetonj.bmp");
		m_s_jetonr = new Sprite(m_t_jetonr);
		m_t_xdra = new Texture("data/xdra.bmp");
		m_s_xdra = new Sprite(m_t_xdra);
		m_s_jetonj = new Sprite(m_t_jetonj);

		m_t_cadre = new Texture("data/cadre.bmp");
		m_s_grille = new Sprite(m_t_grille);
		m_s_grille2 = new Sprite(m_t_grille2);
		m_s_cadre = new Sprite(m_t_cadre);
		m_s_grille.move({DBC,140});
		m_s_grille2.move({DBC,140});
		m_s_cadre.move({DBC-20,120});
		m_s_xdra.move({0, 51});

		m_t_bg = new Texture("data/bg.bmp");
		m_s_bg = new Sprite(m_t_bg);
	}




	public void draw(ref Window win){
		win.draw(m_s_bg);

		win.draw(m_s_grille2);


		if(is_falling == false)
		{
			if(e_where_cursor == TAP1)
			{m_s_jetonr.set_position({DBC, 40});m_s_jetonj.set_position({DBC, 40});}
			else if(e_where_cursor == TAP2)
			{m_s_jetonr.set_position({DBC+80*1, 40});m_s_jetonj.set_position({DBC+80*1, 40});}
			else if(e_where_cursor == TAP3)
			{m_s_jetonr.set_position({DBC+80*2, 40});m_s_jetonj.set_position({DBC+80*2, 40});}
			else if(e_where_cursor == TAP4)
			{m_s_jetonr.set_position({DBC+80*3, 40});m_s_jetonj.set_position({DBC+80*3, 40});}
			else if(e_where_cursor == TAP5)
			{m_s_jetonr.set_position({DBC+80*4, 40});m_s_jetonj.set_position({DBC+80*4, 40});}
			else if(e_where_cursor == TAP6)
			{m_s_jetonr.set_position({DBC+80*5, 40});m_s_jetonj.set_position({DBC+80*5, 40});}
			else if(e_where_cursor == TAP7)
			{m_s_jetonr.set_position({DBC+80*6, 40});m_s_jetonj.set_position({DBC+80*6, 40});}
		}
		else{
			falling_jeton();
		}
		if(m_red_can_play)
			win.draw(m_s_jetonr);
		else
			win.draw(m_s_jetonj);


		foreach (var i in tabjeton)
		{
			if(i != null)
				i.draw(ref win);
		}

		
		win.draw(m_s_grille);
		win.draw(m_s_cadre);
		win.draw(m_s_xdra);
	}




	public void event(ref SDL.Event e){
		if(e.type == SDL.EventType.KEYUP){
			/*if(e.key.keysym.scancode.get_name() == "A"){
			  print("tu as mis pause");
			  }*/

		}
		if(e.type == SDL.EventType.MOUSEBUTTONUP)
		{
			if(e_where_cursor != NULL)
			{
				falling_jeton();
			}
		}
		if(e.type == SDL.EventType.MOUSEMOTION)
		{
			if(e.motion.x > DBC && e.motion.x < DBC+80*1)
				e_where_cursor = TAP1;
			else if(e.motion.x > DBC+80*1 && e.motion.x < DBC+80*2)
				e_where_cursor = TAP2;
			else if(e.motion.x > DBC+80*2 && e.motion.x < DBC+80*3)
				e_where_cursor = TAP3;
			else if(e.motion.x > DBC+80*3 && e.motion.x < DBC+80*4)
				e_where_cursor = TAP4;
			else if(e.motion.x > DBC+80*4 && e.motion.x < DBC+80*5)
				e_where_cursor = TAP5;
			else if(e.motion.x > DBC+80*5 && e.motion.x < DBC+80*6)
				e_where_cursor = TAP6;
			else if(e.motion.x > DBC+80*6 && e.motion.x < DBC+80*7)
				e_where_cursor = TAP7;
			else
				e_where_cursor = NULL;
		}

	}
	protected bool checkp4(){
		//largeur
      Jeton tmp;
      int suite = 0;
      bool debut = true;
      E_COLOR color = RED;
      int x = DBC;
      int y = DBC - 80;
      for (int j = 0; j != 6; j++)
      {
         for(int i = 0; i != 7; i++){
            tmp = get_jeton({x, y});
            if(tmp == null)
            {
               x = x + 80;
               suite = 1;
               debut = false;
               continue;
            }
            if(tmp.get_color() == RED)
            {
               if(debut == true || color == JAUNE)
               {
                   suite = 1;
                   color = RED;
               }
               else
               {
                  suite++;
               }
            }
            else{
               if(debut == true || color == RED)
               {
                  color = JAUNE;
                  suite = 1;
               }
               else
               {
                  suite++;
               }
            }
            if(suite >= 4)
               print("PUISSANCE 4\n");
            debut = false;
            x = x + 80;
         }
         suite = 0;
         debut = true;
         y = y + 80;
         x = DBC;
      }
      
      //hauteur
      suite = 0;
      color = JAUNE;
      debut = true;
      x = DBC;
      y = DBC - 80;
      for (int j = 0; j != 7; j++)
      {
         debut = true;
         for(int i = 0; i != 6; i++){
            tmp = get_jeton({x, y});
            if(tmp == null)
            {
               y = y + 80;
               suite = 1;
               debut = false;
               continue;
            }
            if(tmp.get_color() == RED)
            {
               if(debut == true || color == JAUNE)
               {
                   suite = 1;
                   color = RED;
               }
               else
               {
                  suite++;
               }
            }
            else{
               if(debut == true || color == RED)
               {
                  color = JAUNE;
                  suite = 1;
               }
               else
               {
                  suite++;
               }
            }
            if(suite >= 4)
               print("PUISSANCE 4\n");
            debut = false;
            y = y + 80;
         }
         suite = 0;
         x = x + 80;
         y = DBC - 80;       
      }      
   
      
      return false;
	}


	protected Jeton get_jeton(Vector2i pos){
		foreach (var i in tabjeton){
			if(i == null)
				continue;
			if(i.get_position() == pos)
			   return i;
		}
		return null;
	}



	protected void falling_jeton(){
		is_falling = true;

		if(m_red_can_play)
		{
			m_s_jetonr.move({0,10});
			foreach (var i in tabjeton){
				if(i == null)
					continue;
				if(i.get_position().x != m_s_jetonr.get_position().x)
					continue;
				if(m_s_jetonr.get_position().y == i.get_position().y -80)
				{
					is_falling = false;
					tabjeton[m_nbr_jeton] = new Jeton(ref m_t_jetonr, m_s_jetonr.get_position(), RED);
					m_nbr_jeton++;
					m_red_can_play = false;
					this.checkp4();
					return;
				}
			}
			if(m_s_jetonr.get_position().y > 535){  
				is_falling = false;
				tabjeton[m_nbr_jeton] = new Jeton(ref m_t_jetonr, m_s_jetonr.get_position(), RED);
				m_nbr_jeton++;
				m_red_can_play = false;
				this.checkp4();
			}
		}



		else
		{
			m_s_jetonj.move({0,10});
			foreach (var i in tabjeton){
				if(i == null)
					continue;
				if(i.get_position().x != m_s_jetonj.get_position().x)
					continue;
				if(m_s_jetonj.get_position().y == i.get_position().y -80)
				{
					is_falling = false;
					tabjeton[m_nbr_jeton] = new Jeton(ref m_t_jetonj, m_s_jetonj.get_position(), JAUNE);
					m_nbr_jeton++;
					m_red_can_play = true;
					this.checkp4();
					return;
				}
			}
			if(m_s_jetonj.get_position().y > 535){  
				is_falling = false;
				tabjeton[m_nbr_jeton] = new Jeton(ref m_t_jetonj, m_s_jetonj.get_position(), JAUNE);
				m_nbr_jeton++;
				m_red_can_play = true;
				this.checkp4();
			}
		}
	}



	private Jeton tabjeton[42];
	private int m_nbr_jeton = 0;
	private bool is_falling;
	private WhereCursor e_where_cursor;
	private bool m_curseur;
	private bool m_red_can_play;
	private Texture m_t_jetonr;
	private Sprite m_s_jetonr;
	private Texture m_t_jetonj;
	private Sprite m_s_jetonj;

	private Texture m_t_grille;
	private Texture m_t_grille2;
	private Texture m_t_xdra;
	private Sprite m_s_xdra;
	private Sprite m_s_grille;
	private Sprite m_s_grille2;
	private Texture m_t_cadre;
	private Sprite m_s_cadre;

	private Sprite m_s_bg;
	private Texture m_t_bg;
}

enum WhereCursor{
	NULL,TAP1,TAP2,TAP3,TAP4,TAP5,TAP6,TAP7
}
